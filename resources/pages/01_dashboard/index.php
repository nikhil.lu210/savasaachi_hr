<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>


    <link rel="stylesheet" href="<?= $base_url ?>public/css/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= $base_url ?>public/css/owl/owl.theme.default.min.css">
    <style>
    /* ========< External CSS >======== */
    .t-emp{
        color: #ff7d00;
    }
    .t-dept{
        color: #00bd25;
    }
    .t-awrd{
        color: #13b7ff;
    }
    .t-expns{
        color: #fd05e3;
    }
    .owl-carousel#attendDay .owl-nav{
        position: absolute;
        top: -56px;
        right: 40px;
    }
    .owl-carousel .owl-nav button.owl-next, 
    .owl-carousel .owl-nav button.owl-prev, 
    .owl-carousel button.owl-dot {
        color: #ffffff;
        border: 1px solid #e7e7e73d;
        padding: 3px 10px !important;
        border-radius: 0px;
    }
    .owl-theme .owl-nav {
        font-weight: 500;
        transition: 0.3s all ease-in-out;
    }
    .owl-theme .owl-nav [class*=owl-]:hover,
    .owl-theme .owl-nav [class*=owl-]:focus,
    .owl-theme .owl-nav [class*=owl-]:active {
        background: #ffffff;
        color: #272727;
        font-weight: 500;
        text-decoration: none;
        outline: none;
        transition: 0.3s all ease-in-out;
    }
    .card-header{
        background: #4d0071;
    }
    .card-header h4{
        color: #ffffff;
    }
    i.yes.ontime{
        color: green;
        font-size: 18px;
    }
    i.yes.late{
        color: orangered;
        font-size: 18px;
    }
    i.no{
        color: red;
        font-size: 18px;
    }
    i.holiday{
        color: #999999;
        font-size: 18px;
    }
    i.info{
        color: gray;
        opacity: 0.3;
        font-size: 18px;
    }
    .tooltip-inner {
        padding: 4px 10px;
        border-radius: 1rem;
        /* background: #4d0041; */
        opacity: 10 !important;
    }
    </style>

<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item active">
                            Dashboard
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->


        <div class="row">
            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-bold mb-2 t-emp">22</span>
                            <span class="font-weight-bold">Total Employee</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="fas fa-users t-emp"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-bold mb-2 t-dept">10</span>
                            <span class="font-weight-bold">Total Department</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="fas fa-code-branch t-dept"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-bold mb-2 t-awrd">13</span>
                            <span class="font-weight-bold">Total Award</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="fas fa-award t-awrd"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <span class="h4 d-block font-weight-bold mb-2 t-expns">$320k</span>
                            <span class="font-weight-bold">Total Expense</span>
                        </div>

                        <div class="h2 text-muted">
                            <i class="fas fa-dollar-sign t-expns"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ==========< Attendence Book Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h4 class="text-bold">Attendence (February-2019)</h4>
                    </div>
                    <div class="owl-carousel owl-theme" id="attendDay">
                        <div class="item">
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">01</th>
                                            <th scope="col">02</th>
                                            <th scope="col">03</th>
                                            <th scope="col">04</th>
                                            <th scope="col">05</th>
                                            <th scope="col">06</th>
                                            <th scope="col">07</th>
                                            <th scope="col">08</th>
                                            <th scope="col">09</th>
                                            <th scope="col">10</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Sourav Roy</th>
                                            <td>
                                                <i class="fas fa-check yes late" data-toggle="tooltip" data-placement="top" title="11:20 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-check yes ontime" data-toggle="tooltip" data-placement="top" title="11:00 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-check yes ontime" data-toggle="tooltip" data-placement="top" title="11:00 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-check yes late" data-toggle="tooltip" data-placement="top" title="11:20 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-ban holiday" data-toggle="tooltip" data-placement="top" title="Holiday"></i>
                                            </td> 
                                            <td>
                                                <i class="fas fa-times no" data-toggle="tooltip" data-placement="top" title="Absent"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-times no" data-toggle="tooltip" data-placement="top" title="Absent"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-times no" data-toggle="tooltip" data-placement="top" title="Absent"></i>
                                            </td>
                                            <td>
                                                <i class="far fa-question-circle info"></i>
                                            </td>
                                            <td>
                                                <i class="far fa-question-circle info"></i>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">01</th>
                                            <th scope="col">02</th>
                                            <th scope="col">03</th>
                                            <th scope="col">04</th>
                                            <th scope="col">05</th>
                                            <th scope="col">06</th>
                                            <th scope="col">07</th>
                                            <th scope="col">08</th>
                                            <th scope="col">09</th>
                                            <th scope="col">10</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Sourav Roy</th>
                                            <td>
                                                <i class="fas fa-check yes late" data-toggle="tooltip" data-placement="top" title="11:20 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-check yes ontime" data-toggle="tooltip" data-placement="top" title="11:00 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-check yes ontime" data-toggle="tooltip" data-placement="top" title="11:00 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-check yes late" data-toggle="tooltip" data-placement="top" title="11:20 AM"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-ban holiday" data-toggle="tooltip" data-placement="top" title="Holiday"></i>
                                            </td> 
                                            <td>
                                                <i class="fas fa-times no" data-toggle="tooltip" data-placement="top" title="Absent"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-times no" data-toggle="tooltip" data-placement="top" title="Absent"></i>
                                            </td>
                                            <td>
                                                <i class="fas fa-times no" data-toggle="tooltip" data-placement="top" title="Absent"></i>
                                            </td>
                                            <td>
                                                <i class="far fa-question-circle info"></i>
                                            </td>
                                            <td>
                                                <i class="far fa-question-circle info"></i>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Attendence Book Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script src="<?= $base_url ?>public/js/owl.carousel.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function(){
        $('#attendDay, #attendMonth').owlCarousel({
            loop:false,
            margin:0,
            nav:true,
            navText: [
                '<i class="fas fa-arrow-left"></i>', //Previous
                '<i class="fas fa-arrow-right"></i>' //Next
            ],
            autoHeight: true,
            dots: false,
            center: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1200:{
                    items:1
                }
            }
        })
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>