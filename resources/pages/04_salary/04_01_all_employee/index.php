<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

<link rel="stylesheet" href="<?= $base_url ?>public/css/nice-select.min.css">
<link rel="stylesheet" href="<?= $base_url ?>public/css/yearpicker.css">
<link rel="stylesheet" href="<?= $base_url ?>public/css/data-table.css">

<style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }

    .form-control {
        border-color: #ffffff;
        height: calc(2.25rem + -1px);
    }
    .form-control:focus {
        border-color: #fff;
        border-left: 1px solid #fff;
        transition: 0.5s all ease-in-out;
    }
    #year{
        max-width: 120px;
    }
    .nice-select{
        float: unset;
        border-radius: 0px;
        height: calc(2.0625rem + 2px);
        padding-top: 0px;
        min-width: 150px;
        line-height: 34px;
    }
    .nice-select.open .list{
        width: 100%;
    }
    .nice-select.open, .nice-select:active, .nice-select:focus, .nice-select:hover {
        border-color: #fff;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index.php"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Salary
                        </li>
                        <li class="breadcrumb-item active">
                            All Employee
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Salary All Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>Monthly Salary</h4>
                        </div>
                        <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/create.php" class="btn btn-outline-light btn-custom float-right">Control Salary</a>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header date-filter">
                                <h4 class="float-left">February 2019</h4>
                                <div class="float-right">
                                    <form class="form-inline" action="" method="post">
                                    <div class="form-group">
                                        <input type="text" id="year" name="year" class="form-control yearpicker" placeholder="2019">
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group">
                                            <select id="month" name="month" class="form-control" required>
                                                <option>Select Month</option>
                                                <option value="1">January</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-light btn-custom btn-filter">Filter</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Basic Salary</th>
                                            <th scope="col">Current Salary</th>
                                            <th scope="col">Total Salary</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Sourav Roy Avijeet</th>
                                            <td>6000</td>
                                            <td>10000</td>
                                            <th scope="row">9550</th>
                                            <td>
                                                <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/show.php" class="btn btn-light btn-custom btn-view btn-sm">View</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Salary All Employee Ends >============ -->
    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

<script src="<?= $base_url ?>public/js/nice-select.min.js"></script>
<script src="<?= $base_url ?>public/js/yearpicker.js"></script>
    <script src="<?= $base_url ?>public/js/jquery.dataTables.min.js"></script>
    <script src="<?= $base_url ?>public/js/dataTables.bootstrap.min.js"></script>

    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function() {
        $('select').niceSelect();

        $('#dataTable').DataTable();

        // $('.yearpicker').yearpicker();
        $('.yearpicker').yearpicker({

            // Auto Hide
            autoHide: true,

            // Initial Year
            year: null,

            // Start Year
            startYear: null,

            // End Year
            endYear: null,

            // Element tag
            itemTag: 'li',

            // Default CSS classes
            selectedClass: 'selected',
            disabledClass: 'disabled',
            hideClass: 'hide',
            highlightedClass: 'highlighted',

            // Custom template
            template: `
                <div class="yearpicker-container">
                    <div class="yearpicker-header">
                        <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                        <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                        <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
                    </div>
                    <div class="yearpicker-body">
                        <ul class="yearpicker-year" data-view="years">
                        </ul>
                    </div>
                </div>
            `,
            show: null,
            hide: null,
            pick: null

        });
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>