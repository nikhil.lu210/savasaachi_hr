<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>
    
    
    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .btn-plus{
        margin-top: -6px;
        margin-bottom: -6px;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index.php"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Salary
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/index.php">All Employee</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/show.php">Sourav Roy Avijeet</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Update Salary
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Update Sourav Roy Avijeet's Salary</h4>
                            </div>
                            <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                        </div>
                        <div class="card-body">
                            
                            <div class="row">
                                <!-- Department -->
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>Overtime Work <sup>(March)</sup></h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="overtime" class="form-control-label">Overtime *<sup>(hour)</sup> </label>
                                                        <input type="text" id="overtime" name="overtime" class="form-control removeDis" disabled value="13" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Department -->
                                
                                <!-- Designation -->
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6 class="float-left">Extra Bonus <sup>(March)</sup></h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row" id="designation">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="extra_bonus" class="form-control-label">Extra Bonus <sup>(taka)</sup></label>
                                                        <input type="text" id="extra_bonus" name="extra_bonus" class="form-control removeDis" disabled value="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Designation -->
                            </div>
                            
                        </div>
                        <div class="card-footer">
                            <div class="btn-group float-right" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-light btn-custom btn-edit" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>

                                <button type="button" class="btn btn-light btn-custom btn-cancle hiddenButton d-none" id="addDisabled"><i class="fas fa-redo-alt"></i> Cancel</button>

                                <button type="submit" class="btn btn-light btn-custom hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function(){
        $("#removeDisabled").click(function(){
            $('.removeDis').removeAttr('disabled', 'disabled');
            $('.removeDis').removeClass('disabled');
            $('#removeDisabled').addClass('d-none');
            $('.hiddenButton').removeClass('d-none');
        
        });
        
        $("#addDisabled").click(function(){
            $('.removeDis').attr('disabled', 'disabled');
            $('.removeDis').addClass('disabled');
            $('#removeDisabled').removeClass('d-none');
            $('.hiddenButton').addClass('d-none');
        
        });
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>