<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

<style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-footer h4{
        color: #ffffff;
        padding-top: 8px;
    }
    .card-footer h4:nth-child(1){
        padding-left: 40px;
    }
    .card-footer h4:nth-child(2){
        padding-right: 40px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .btn-group {
        margin-top: 0px;
    }
    table th{
        width: 50%;
        opacity: 0.7;
    }
    table tr{
        width: 50%;
        font-weight: 700;
        opacity: 1;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index.php"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Salary
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/index.php">All Employee</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Sourav Roy Avijeet
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Salary All Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>Sourav Roy Avijeet</h4>
                        </div>
                        <div class="btn-group float-right" role="group">
                            <a href="javascript:history.back()" class="btn btn-light btn-custom">Back</a>
                            
                            <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/update.php" class="btn btn-outline-light btn-custom">Update</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <th>Total Holiday</th>
                                            <td>6 Days</td>
                                        </tr>
                                        <tr>
                                            <th>Casual leave</th>
                                            <td>1 Days</td>
                                        </tr>
                                        <tr>
                                            <th>Sick leave</th>
                                            <td>1 Days</td>
                                        </tr>
                                        <tr>
                                            <th>Paid leave</th>
                                            <td>3 Days</td>
                                        </tr>
                                        <tr>
                                            <th>Festival Bonus</th>
                                            <td>0 Taka</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <th>Extra Leave Hour</th>
                                            <td>7 Hours</td>
                                        </tr>
                                        <tr>
                                            <th>Overtime</th>
                                            <td>72 Hours</td>
                                        </tr>
                                        <tr>
                                            <th>Total Attendance</th>
                                            <td>160 Hours</td>
                                        </tr>
                                        <tr>
                                            <th>Total Absent</th>
                                            <td>48 Hours</td>
                                        </tr>
                                        <tr>
                                            <th>Extra Bonus</th>
                                            <td>0 Taka</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <h4 class="float-left">Total Salary of March </h4>
                        <h4 class="float-right">10000 tk</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Salary All Employee Ends >============ -->
    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script>
    /* ========< External JS >======== -->*/
    
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>