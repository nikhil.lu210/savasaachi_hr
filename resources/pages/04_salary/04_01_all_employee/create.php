<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>
    <link href="<?= $base_url ?>public/css/select2.min.css" rel="stylesheet" />
    
    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .btn-plus{
        margin-top: -6px;
        margin-bottom: -6px;
    }
    
    .select2-container .select2-selection--single{
        height: 38px;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow{
        top: 5px;
    }
    .select2-container--default .select2-selection--single{
        border-color: #e8e8e8;
        border-radius: 0px;
        position: relative;
    }
    .select2-container--default .select2-selection--single:focus{
        outline: none !important;
        box-shadow: none !important;
        border-color: #4d0071;
        border-left: 3px solid #4d0071;
        transition: 0.5s all ease-in-out;
    }
    .select2-container--default .select2-search--dropdown .select2-search__field:focus{
        outline: none !important;
        box-shadow: none !important;
        border-color: #4d0071;
        border-left: 3px solid #4d0071;
        transition: 0.5s all ease-in-out;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
        background-color: #4d0071;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index.php"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Salary
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/index.php">All Employee</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Control Salary
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Control Salary</h4>
                            </div>
                            <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Employee Name *</label>
                                        <select id="name" name="name" class="form-control select-two" required>
                                            <option>Select Employee</option>
                                            <option value="">Tanjil Haque Fahim</option>
                                            <option value="">Sourav Roy Avijeet</option>
                                            <option value="">Sujan Baidya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="overtime" class="form-control-label">Overtime * <sup>Hour</sup></label>
                                        <input type="text" id="overtime" name="overtime" class="form-control" placeholder="21" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="extra_bonus" class="form-control-label">Extra Bonus <sup>Taka</sup></label>
                                        <input type="text" id="extra_bonus" name="extra_bonus" class="form-control" placeholder="2500" required>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-light btn-custom btn-submit float-right" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>
    <script src="<?= $base_url ?>public/js/select2.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function() {
        $('.select-two').select2();
        $('.select2-container').css("width", "100%");
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>