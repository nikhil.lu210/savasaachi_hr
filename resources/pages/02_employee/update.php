<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css">
    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .nice-select{
        float: unset;
        border-radius: 0px;
        height: 40px;
        height: calc(2.0625rem + 5px);
        padding-top: 0px;
    }
    .nice-select.open .list{
        width: 100%;
    }
    .nice-select.open, .nice-select:active, .nice-select:focus {
        border-color: #4d0071;
    }
    
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Employee</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/02_employee/show">Sourav Roy Avijeet</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Update Profile
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Update Profile</h4>
                            </div>
                            <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                        </div>
                        <div class="card-body">

                            <!-- Personal Starts -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>Personal Information</h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="name" class="form-control-label">Full Name *</label>
                                                        <input type="text" id="name" name="name" class="form-control removeDis" disabled value="Jhon Doe" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fathers_name" class="form-control-label">Father's Name *</label>
                                                        <input type="text" id="fathers_name" name="fathers_name" class="form-control removeDis" disabled value="Jhon Doe" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fathers_number" class="form-control-label">Father's Mobile No</label>
                                                        <input type="tel" id="fathers_number" name="fathers_number" class="form-control removeDis" disabled value="+8801234567890">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="birthdate" class="form-control-label">Date Of Birth *</label>
                                                        <input type="text" id="birthdate" name="birthdate" class="form-control removeDis" disabled value="27-10-1995" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="gender">Gender *</label>
                                                        <select id="gender" name="gender" class="form-control removeDis" disabled required>
                                                            <option>Select Gender</option>
                                                            <option value="male">Male</option>
                                                            <option value="female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="email" class="form-control-label">Email *</label>
                                                        <input type="email" id="email" name="email" class="form-control removeDis" disabled value="jhondoe@gmail.com" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="phone1" class="form-control-label">Phone Number *</label>
                                                        <input type="tel" id="phone1" name="phone1" class="form-control removeDis" disabled value="+8801234567890" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="phone2" class="form-control-label">Another Phone</label>
                                                        <input type="tel" id="phone2" name="phone2" class="form-control removeDis" disabled value="+8801234567890">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="nid_no" class="form-control-label">NID No *</label>
                                                        <input type="text" id="nid_no" name="nid_no" class="form-control removeDis" disabled value="199501212425346457" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="facebook" class="form-control-label">Facebook</label>
                                                        <input type="url" id="facebook" name="facebook" class="form-control removeDis" disabled value="https://www.facebook.com/username">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="linkedin" class="form-control-label">Linkedin</label>
                                                        <input type="url" id="linkedin" name="linkedin" class="form-control removeDis" disabled value="https://www.linkedin.com/in/username">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="git" class="form-control-label">Github / Gitlab</label>
                                                        <input type="url" id="git" name="git" class="form-control removeDis" disabled value="https://github.com/username">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="present_address">Present Address *</label>
                                                        <textarea id="present_address" name="present_address" class="form-control removeDis" disabled rows="2" value="Present Address" required>Present Address</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="permanent_address">Permanent Address *</label>
                                                        <textarea id="permanent_address" name="permanent_address" class="form-control removeDis" disabled rows="2" value="Permanent Address" required>Permanent Address</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Personal Ends -->

                            <!-- Company Starts -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>Company Information</h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="employee_id" class="form-control-label">Employee ID *</label>
                                                        <input type="text" id="employee_id" name="employee_id" class="form-control removeDis" disabled value="Jhon Doe" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="basic_salary" class="form-control-label">Basic Salary *</label>
                                                        <input type="text" id="basic_salary" name="basic_salary" class="form-control removeDis" disabled value="10000" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="department">Department *</label>
                                                        <select id="department" name="department" class="form-control removeDis" disabled required>
                                                            <option>Select Department</option>
                                                            <option value="1">Posting</option>
                                                            <option value="2">Content</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="designation">Designation *</label>
                                                        <select id="designation" name="designation" class="form-control removeDis" disabled required>
                                                            <option>Select Designation</option>
                                                            <option value="1">Posting Manager</option>
                                                            <option value="2">Posting Assistance</option>
                                                            <option value="3">Content Manager</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="join_date" class="form-control-label">Joining Date*</label>
                                                        <input type="text" id="join_date" name="join_date" class="form-control removeDis" disabled value="27-10-1995" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="end_date" class="form-control-label">Ending Date*</label>
                                                        <input type="text" id="end_date" name="end_date" class="form-control removeDis" disabled value="27-10-1995" required>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input removeDis" disabled name="working" id="wotking">
                                                        <label class="form-check-label" for="wotking">Currently Working</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Company Ends -->

                            <!-- Bank Account Starts -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>Banking Information</h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="ac_holder_name" class="form-control-label">Account Holder Name *</label>
                                                        <input type="text" id="ac_holder_name" name="ac_holder_name" class="form-control removeDis" disabled value="Jhon Doe" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="bank_name" class="form-control-label">Bank Name *</label>
                                                        <input type="text" id="bank_name" name="bank_name" class="form-control removeDis" disabled value="UCB Bank Ltd." required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="branch_name" class="form-control-label">Branch Name *</label>
                                                        <input type="text" id="branch_name" name="branch_name" class="form-control removeDis" disabled value="Lamabazar, Sylhet" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="ac_no" class="form-control-label">Account Number *</label>
                                                        <input type="text" id="ac_no" name="ac_no" class="form-control removeDis" disabled value="23454687693523464575" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="ifsc_code" class="form-control-label">IFSC Code *</label>
                                                        <input type="text" id="ifsc_code" name="ifsc_code" class="form-control removeDis" disabled value="123 412 53" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="pan_number" class="form-control-label">PAN Number *</label>
                                                        <input type="text" id="pan_number" name="pan_number" class="form-control removeDis" disabled value="12323 412345 543" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Bank Account Ends -->

                            <!-- Documents Starts -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>External Documents</h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="avatar" class="form-control-label">Avatar *</label>
                                                        <input type="file" id="avatar" name="avatar" class="form-control removeDis" disabled value="Avatar" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cv_resume" class="form-control-label">CV / Resume</label>
                                                        <input type="file" id="cv_resume" name="cv_resume" class="form-control removeDis" disabled value="CV / Resume">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="appointment_letter" class="form-control-label">Appointment Letter</label>
                                                        <input type="file" id="appointment_letter" name="appointment_letter" class="form-control removeDis" disabled value="CV / Resume">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="id_proof" class="form-control-label">ID Proof</label>
                                                        <input type="file" id="id_proof" name="id_proof" class="form-control removeDis" disabled value="CV / Resume">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Documents Ends -->

                        </div>
                        <div class="card-footer">
                            <div class="btn-group float-right" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-light btn-custom btn-edit" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>

                                <button type="button" class="btn btn-light btn-custom btn-cancle hiddenButton d-none" id="addDisabled"><i class="fas fa-redo-alt"></i> Cancel</button>

                                <button type="submit" class="btn btn-light btn-custom hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function () {
        var checkbox = $('#wotking');
        var endDate = $('#end_date');

        $('input').on('click',function () {
            if (checkbox.is(':checked')) {
                endDate.attr('disabled', 'true');
            } else {
                endDate.removeAttr('disabled');
            }
        });
    });

    $(document).ready(function() {
        $('select').niceSelect();
    });

    $(document).ready(function(){
        $("#removeDisabled").click(function(){
            $('.removeDis').removeAttr('disabled', 'disabled');
            $('.removeDis').removeClass('disabled');
            $('#removeDisabled').addClass('d-none');
            $('.hiddenButton').removeClass('d-none');
        
        });
        
        $("#addDisabled").click(function(){
            $('.removeDis').attr('disabled', 'disabled');
            $('.removeDis').addClass('disabled');
            $('#removeDisabled').removeClass('d-none');
            $('.hiddenButton').addClass('d-none');
        
        });
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>