<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

<link rel="stylesheet" href="<?= $base_url ?>public/css/data-table.css">
    <style>
    /* ========< External CSS >======== */
    .card.employee .card-footer{
        padding-left: 30px;
        padding-right: 30px;
    }
    .card-body{
        padding-bottom: 0px;
    }
    .card-header{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-body img{
        max-width: 50px;
        max-height: 50px;
        border-radius: 5px;
        border: 1px solid #4d00712f;
    }
    .card-body h5{
        color: #000000;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .card-body ul{
        padding: 0px;
    }
    .card-body ul>li{
        list-style: none;
        font-size: 16px;
        font-weight: 600;
        color: #666666;
        padding: 1px 0px;
    }
    .status.active{
        color: #0bc704;
        font-size: 20px;
        padding-top: 5px;
        padding-right: 7px;
    }
    .status.inactive{
        color: #ff0d0d;
        font-size: 20px;
        padding-top: 5px;
        padding-right: 7px;
    }

    tr{
        border-bottom: 1px solid #efefef52;
    }
    tr:last-child{
        border-bottom: 0px solid #efefef52;
    }
    th.basic-td,
    td.basic-td{
        padding-top: 20px !important;
    }
    td.image-td{
        padding-top: 7px;
        padding-bottom: 7px;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item active">
                            Employee
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>Total Employee</h4>
                        </div>
                        <a href="<?= $base_url ?>resources/pages/02_employee/create" class="btn btn-outline-light btn-custom float-right">Add New Employee</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Avatar</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Designation</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="basic-td" scope="row">SAVA2019001</th>
                                    <td class="image-td">
                                        <img src="<?= $base_url ?>public/images/sourav_avatar.jpg" alt="" class="img-responsive mx-auto d-block">
                                    </td>
                                    <td class="basic-td">Sourav Roy Avijeet</td>
                                    <td class="basic-td">Web Application Developer</td>
                                    <td class="basic-td">+8801234567891</td>
                                    <td class="basic-td text-center">
                                        <i class="far fa-dot-circle status active"></i>
                                    </td>
                                    <td class="basic-td">
                                        <a href="<?= $base_url ?>resources/pages/02_employee/show" class="btn btn-light btn-custom btn-view btn-sm">View</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="card-body d-none">
                        <div class="row">

                            <!-- Employee Starts -->
                            <div class="col-md-4">
                                <div class="card employee">
                                    <div class="card-body text-center">
                                        <div class="status active">
                                            <i class="far fa-dot-circle"></i>
                                        </div>

                                        <img src="<?= $base_url ?>public/images/admin.png" alt="" class="img-responsive mx-auto d-block">

                                        <h5 class="name">Mr. Sourav Roy Avijeet</h5>
                                        
                                        <ul>
                                            <li class="designation">D/N: Web Application Developer</li>
                                            <li class="id">ID: SAVA2019001</li>
                                            <li class="phone">Cell: +88012345678</li>
                                            <li class="email">Email: jhon.doe@mail.com</li>
                                        </ul>
                                    </div>
                                    <div class="card-footer">
                                        <a href="#" class="btn btn-outline-light btn-custom btn-delete btn-sm float-left" onclick="return confirm('Are you sure you want to delete this employee?');">Delete</a>

                                        <a href="<?= $base_url ?>resources/pages/02_employee/show" class="btn btn-light btn-custom btn-view btn-sm float-right">View</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Employee Ends -->
                            

                            <!-- Employee Starts -->
                            <div class="col-md-4">
                                <div class="card employee">
                                    <div class="card-body text-center">
                                        <div class="status inactive"><i class="far fa-dot-circle"></i></div>
                                        
                                        <img src="<?= $base_url ?>public/images/admin.png" alt="" class="img-responsive mx-auto d-block">
                                        <h5 class="name">Mr. Sourav Roy Avijeet</h5>
                                        <ul>
                                            <li class="designation">D/N: Web Application Developer</li>
                                            <li class="id">ID: SAVA2019001</li>
                                            <li class="phone">Cell: +88012345678</li>
                                            <li class="email">Email: jhon.doe@mail.com</li>
                                        </ul>
                                    </div>
                                    <div class="card-footer">
                                        <a href="#" class="btn btn-outline-light btn-custom btn-delete btn-sm float-left" onclick="return confirm('Are you sure you want to delete this employee?');">Delete</a>

                                        <a href="#" class="btn btn-light btn-custom btn-view btn-sm float-right">View</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Employee Ends -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script src="<?= $base_url ?>public/js/jquery.dataTables.min.js"></script>
    <script src="<?= $base_url ?>public/js/dataTables.bootstrap.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function(){
        $('#dataTable').DataTable();
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>