<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css">
    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .profile-avatar{
        max-width: 80%;
        max-height: 80%;
        border-radius: 5%;
    }
    .social-links ul{
        padding-left: 0px;
    }
    .social-links ul>li{
        display: inline-flex;
        list-style: none;
        margin-right: 20px;
        /* margin-left: 14px; */
    }
    .social-links ul>li a{
        font-size: 14px;
        font-weight: normal;
        color: #ffffff;
        border-radius: 5px;
    }
    .social-links ul>li.facebook a{
        padding: 5px 12px;
        background: #3b5998;
        border: 1px solid #3b5998;
    }
    .social-links ul>li.linkedin a{
        padding: 5px 10px;
        background: #007bb5;
        border: 1px solid #007bb5;
    }
    .social-links ul>li.git a{
        padding: 5px 10px;
        background: #333;
        border: 1px solid #333;
    }
    th{
        width: 30%;
        opacity: 0.6;
    }
    td{
        width: 70%;
        font-weight: 700;
    }
    td a{
        color: #4d0071cf;
        transition: 0.3s all ease-in-out;
    }
    td a:hover,
    td a:active,
    td a:focus{
        color: #4d0071;
        transition: 0.3s all ease-in-out;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Employee</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Sourav Roy Avijeet
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Sourav Roy Avijeet</h4>
                            </div>
                            <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                        </div>
                        <div class="card-body">
                            <!-- Personal Details -->
                            <div class="card">
                                <div class="card-header">
                                    <h6>Personal Details</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <figure>
                                                <img src="<?= $base_url ?>public/images/sourav_avatar.jpg" alt="" class="img-responsive img-thumbnail profile-avatar">
                                            </figure>

                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th>Name: </th>
                                                        <td>Jhon Doe</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Phone: </th>
                                                        <td>+8801234567890</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email: </th>
                                                        <td>email@mail.com</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Links: </th>
                                                        <td>
                                                            <div class="social-links">
                                                                <ul>
                                                                    <li class="facebook">
                                                                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                                                    </li>
                                                                    <li class="linkedin">
                                                                        <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                                    </li>
                                                                    <li class="git">
                                                                        <a href="#" target="_blank"><i class="fab fa-git"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-7">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th>Mobile: </th>
                                                        <td>+8801234567891</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Date of Birth: </th>
                                                        <td>27-10-1995</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Gender: </th>
                                                        <td>Male</td>
                                                    </tr>
                                                    <tr>
                                                        <th>NID: </th>
                                                        <td>1234576894786978045</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Father's Name: </th>
                                                        <td>Mr. Habib Quersy Doe</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Father's Mobile: </th>
                                                        <td>+8801234567892</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Present Address: </th>
                                                        <td>123, West Evenue Road, USA</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Permanent Address: </th>
                                                        <td>123, West Evenue Road, USA
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>ID Prrof: </th>
                                                        <td>
                                                            <a href="#" target="_blank"><i class="fas fa-cloud-download-alt"></i> Download ID Proof</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Basic Salary:</th>
                                                        <td>10000 tk</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Personal Details -->

                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Company Details -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>Company Details</h6>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th>Employee ID: </th>
                                                        <td>00281</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Department: </th>
                                                        <td>Posting</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Designation: </th>
                                                        <td>Posting Manager</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Joining Date: </th>
                                                        <td>01-01-2018</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ending Date: </th>
                                                        <td>Currently Working</td>
                                                    </tr>
                                                    <tr>
                                                        <th>CV / Resume: </th>
                                                        <td>
                                                            <a href="#" target="_blank"><i class="fas fa-cloud-download-alt"></i> Download CV / Resume</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Appoinment: </th>
                                                        <td>
                                                            <a href="#" target="_blank"><i class="fas fa-cloud-download-alt"></i> Download Appoinment Letter</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- Company Details -->
                                </div>
                                <div class="col-md-6">
                                    <!-- Bank Details -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>Bank Details</h6>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th>AC Holder Name:</th>
                                                        <td>Mr. Jhon Doe</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Bank Name:</th>
                                                        <td>UCB Bank</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Branch Name:</th>
                                                        <td>Lamabazar, Sylhet</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Account Name:</th>
                                                        <td>01029003545645</td>
                                                    </tr>
                                                    <tr>
                                                        <th>IFSC Code:</th>
                                                        <td>123 412 53</td>
                                                    </tr>
                                                    <tr>
                                                        <th>PAN Number:</th>
                                                        <td>1234 45678 890</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- Bank Details -->
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <a href="<?= $base_url ?>resources/pages/02_employee/update" class="btn btn-light btn-custom btn-submit float-right">Update Profile</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>