<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>
    
    
    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .btn-plus{
        margin-top: -6px;
        margin-bottom: -6px;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Profile
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/06_menu/06_01_profile/index">Admin Name</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Update Information
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Admin Name</h4>
                            </div>
                            <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                        </div>
                        <div class="card-body">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="form-control-label">Name</label>
                                        <input type="text" id="name" name="name" class="form-control removeDis" disabled value="Admin Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="profile_picture" class="form-control-label">Profile Picture</label>
                                        <input type="file" id="profile_picture" name="profile_picture" class="form-control removeDis" disabled value="Profile Picture">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="mobile_no" class="form-control-label">Mobile No.</label>
                                        <input type="text" id="mobile_no" name="mobile_no" class="form-control removeDis" disabled value="+8801712345678">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email" class="form-control-label">Email</label>
                                        <input type="text" id="email" name="email" class="form-control removeDis" disabled value="admin.veechi@gmail.com">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="position" class="form-control-label">Company Position</label>
                                        <input type="text" id="position" name="position" class="form-control removeDis" disabled value="CEO">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="card-footer">
                            <div class="btn-group float-right" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-light btn-custom btn-edit" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>

                                <button type="button" class="btn btn-light btn-custom btn-cancle hiddenButton d-none" id="addDisabled"><i class="fas fa-redo-alt"></i> Cancel</button>

                                <button type="submit" class="btn btn-light btn-custom hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function(){
        $("#removeDisabled").click(function(){
            $('.removeDis').removeAttr('disabled', 'disabled');
            $('.removeDis').removeClass('disabled');
            $('#removeDisabled').addClass('d-none');
            $('.hiddenButton').removeClass('d-none');
        
        });
        
        $("#addDisabled").click(function(){
            $('.removeDis').attr('disabled', 'disabled');
            $('.removeDis').addClass('disabled');
            $('#removeDisabled').removeClass('d-none');
            $('.hiddenButton').addClass('d-none');
        
        });
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>