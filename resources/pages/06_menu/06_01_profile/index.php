<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .profile-avatar{
        max-width: 80%;
        max-height: 80%;
        border-radius: 5%;
    }
    .social-links ul{
        padding-left: 0px;
    }
    .social-links ul>li{
        display: inline-flex;
        list-style: none;
        margin-right: 20px;
        /* margin-left: 14px; */
    }
    .social-links ul>li a{
        font-size: 14px;
        font-weight: normal;
        color: #ffffff;
        border-radius: 5px;
    }
    .social-links ul>li.facebook a{
        padding: 5px 12px;
        background: #3b5998;
        border: 1px solid #3b5998;
    }
    .social-links ul>li.linkedin a{
        padding: 5px 10px;
        background: #007bb5;
        border: 1px solid #007bb5;
    }
    .social-links ul>li.git a{
        padding: 5px 10px;
        background: #333;
        border: 1px solid #333;
    }
    th{
        width: 30%;
        opacity: 0.6;
    }
    td{
        width: 70%;
        font-weight: 700;
    }
    td a{
        color: #4d0071cf;
        transition: 0.3s all ease-in-out;
    }
    td a:hover,
    td a:active,
    td a:focus{
        color: #4d0071;
        transition: 0.3s all ease-in-out;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Profile
                        </li>
                        <li class="breadcrumb-item active">
                            Admin Name
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Admin Name</h4>
                            </div>
                            <a href="<?= $base_url ?>resources/pages/06_menu/06_01_profile/update" class="btn btn-outline-light btn-custom float-right">Update</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <figure>
                                        <img src="<?= $base_url ?>public/images/admin.png" alt="" class="img-responsive img-thumbnail profile-avatar">
                                    </figure>
                                </div>

                                <div class="col-md-7">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th>Name: </th>
                                                <td>Admin Name</td>
                                            </tr>
                                            <tr>
                                                <th>Mobile No: </th>
                                                <td>+8801712345678</td>
                                            </tr>
                                            <tr>
                                                <th>Email: </th>
                                                <td>admin.veechi@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <th>Company Position: </th>
                                                <td>CEO</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>