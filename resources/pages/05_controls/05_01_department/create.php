<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>
    
    
    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .btn-plus{
        margin-top: -6px;
        margin-bottom: -6px;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Controls
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/05_controls/05_01_department/index">Department</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Add New Department
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Add New Department</h4>
                            </div>
                            <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                        </div>
                        <div class="card-body">
                            
                            <div class="row">
                                <!-- Department -->
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>Department</h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="dept_name" class="form-control-label">Department Name *</label>
                                                        <input type="text" id="dept_name" name="dept_name" class="form-control" placeholder="Administration" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Department -->
                                
                                <!-- Designation -->
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h6 class="float-left">Designation</h6>
                                            <a href="#" class="btn btn-outline-light btn-custom btn-plus btn-sm float-right"><i class="fas fa-plus"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="row" id="designation">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="dept_name" class="form-control-label">Designation Name</label>
                                                        <input type="text" id="dept_name" name="dept_name" class="form-control" placeholder="Assistant Administrative Officer">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Designation -->
                            </div>
                            
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-light btn-custom btn-submit float-right" type="submit">Add New Department</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script>
    /* ========< External JS >======== -->*/
    $(".btn-plus").click(function () {
        $("#designation").append('<div class="col-md-12"><div class="form-group"><label for="dept_name" class="form-control-label">Designation Name</label><input type="text" id="dept_name" name="dept_name" class="form-control" placeholder="Assistant Administrative Officer"></div></div>');
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>