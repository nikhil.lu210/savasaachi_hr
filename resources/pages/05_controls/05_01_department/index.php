<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

<link rel="stylesheet" href="<?= $base_url ?>public/css/data-table.css">
<style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .card-header.date-filter{
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .form-control {
        border-color: #ffffff;
        height: calc(2.25rem + -1px);
    }
    .form-control:focus {
        border-color: #fff;
        border-left: 1px solid #fff;
        transition: 0.5s all ease-in-out;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Controls
                        </li>
                        <li class="breadcrumb-item active">
                            Department
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Department Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>Daily Attendance</h4>
                        </div>
                        <a href="<?= $base_url ?>resources/pages/05_controls/05_01_department/create" class="btn btn-outline-light btn-custom float-right">Add New</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th scope="col">SL.</th>
                                    <th scope="col">Department Name</th>
                                    <th scope="col">Total Designation</th>
                                    <th scope="col">Total Employee</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">01</th>
                                    <td>Administration</td>
                                    <td>03</td>
                                    <td>07</td>
                                    <td>
                                        <a href="#" class="btn btn-light btn-custom btn-view btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">02</th>
                                    <td>Campaigns Management</td>
                                    <td>07</td>
                                    <td>22</td>
                                    <td>
                                        <a href="#" class="btn btn-light btn-custom btn-view btn-sm">View</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Department Ends >============ -->
    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

<script src="<?= $base_url ?>public/js/jquery.dataTables.min.js"></script>
<script src="<?= $base_url ?>public/js/dataTables.bootstrap.min.js"></script>

    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>