<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>
    
    <link rel="stylesheet" href="<?= $base_url ?>public/css/nice-select.min.css">
    <link rel="stylesheet" href="<?= $base_url ?>public/css/bootstrapDatepickr-1.0.0.min.css">
    <style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .nice-select{
        float: unset;
        border-radius: 0px;
        height: 40px;
        height: calc(2.0625rem + 5px);
        padding-top: 0px;
    }
    .nice-select.open .list{
        width: 100%;
    }
    .nice-select.open, .nice-select:active, .nice-select:focus {
        border-color: #4d0071;
    }
    
    .bootstrapDatepickr-day:hover, .bootstrapDatepickr-next-month:hover, .bootstrapDatepickr-prev-month:hover {
        background-color: #4d0071;
        color: #fff;
    }

    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Controls
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/05_controls/05_04_expense/index">Expense</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Add New Expense
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Holiday Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <h4>Assign New Expense</h4>
                            </div>
                            <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                        </div>
                        <div class="card-body">

                            <!-- New Expense Starts -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date" class="form-control-label">Date *</label>
                                        <input type="text" id="date" name="date" class="form-control" placeholder="21-02-2019" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="product" class="form-control-label">Product *</label>
                                        <input type="text" id="product" name="product" class="form-control" placeholder="Printer" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type">Type *</label>
                                        <select id="type" name="type" class="form-control" required>
                                            <option>Select Type</option>
                                            <option value="1">Type 01</option>
                                            <option value="2">Type 02</option>
                                            <option value="3">Type 03</option>
                                            <option value="4">Type 04</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cost" class="form-control-label">Total Cost *</label>
                                        <input type="text" id="cost" name="cost" class="form-control" placeholder="750" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="note">Note</label>
                                        <textarea id="note" name="note" class="form-control" rows="3" placeholder="Note"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- New Expense Ends -->

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-light btn-custom btn-submit float-right">Add Holiday</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ===========< Holiday Ends >============ -->

    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script src="<?= $base_url ?>public/js/nice-select.min.js"></script>
    <script src="<?= $base_url ?>public/js/bootstrapDatepickr-1.0.0.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function () {
        // Nice Select
        $('select').niceSelect();

        // Date Picker
        $("#date").bootstrapDatepickr({
            date_format: "d-m-Y"
        });
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>