<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

<style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 150px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    h1{
        font-size: 72px;
        padding-top: 100px;
        color: #4d0071;
        text-transform: uppercase;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index.php"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            Controls
                        </li>
                        <li class="breadcrumb-item active">
                            Notice
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Department Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>Notice</h4>
                        </div>
                        <a href="" class="btn btn-outline-light btn-custom float-right">Add New Notice</a>
                    </div>
                    <div class="card-body">
                        <!-- <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row"></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table> -->
                        <h1 class="text-center">No Notice To Show</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Department Ends >============ -->
    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script>
    /* ========< External JS >======== -->*/
    
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>