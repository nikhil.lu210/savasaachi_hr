<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

<link rel="stylesheet" href="<?= $base_url ?>public/css/bootstrapDatepickr-1.0.0.min.css">
<link rel="stylesheet" href="<?= $base_url ?>public/css/data-table.css">
<style>
    /* ========< External CSS >======== */
    .card-body{
        padding-bottom: 0px;
    }
    .card-header, .card-footer{
        background: #4d0071;
    }
    .card-footer{
        padding-top: 5px;
    }
    .card-header h4{
        color: #ffffff;
        padding-top: 4px;
    }
    .card-header h6{
        color: #ffffff;
        text-align:left;
        text-transform: uppercase;
    }
    .card-header.date-filter{
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .form-control {
        border-color: #ffffff;
        height: calc(2.25rem + -1px);
    }
    .form-control:focus {
        border-color: #fff;
        border-left: 1px solid #fff;
        transition: 0.5s all ease-in-out;
    }
    .bootstrapDatepickr-cal{
        left: auto !important;
        right: 16px !important;
        width: 50% !important;
        top: 45px;
    }
    .bootstrapDatepickr-day:hover, .bootstrapDatepickr-next-month:hover, .bootstrapDatepickr-prev-month:hover {
        background-color: #4d0071;
        color: #fff;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item active">
                            Attendance
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Total Employee Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>Daily Attendance</h4>
                        </div>
                        <a href="<?= $base_url ?>resources/pages/03_attendance/create.php" class="btn btn-outline-light btn-custom float-right">Assign Today</a>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header date-filter">
                                <h4 class="float-left">14-02-2019</h4>
                                <div class="float-right">
                                    <form class="form-inline" action="" method="post">
                                    <div class="form-group">
                                        <input type="text" id="date" name="date" class="form-control" placeholder="14-02-2019">
                                        <button type="submit" class="btn btn-light btn-custom btn-filter">Filter</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Checked In</th>
                                            <th scope="col">Note</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Sourav Roy</th>
                                            <td>Present</td>
                                            <td>11.20 AM</td>
                                            <td>Late</td>
                                            <td>
                                                <a href="#" class="btn btn-light btn-custom btn-view btn-sm">View</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Jakaria Hussain</th>
                                            <td>Absent</td>
                                            <td></td>
                                            <td>Leave</td>
                                            <td>
                                                <a href="#" class="btn btn-light btn-custom btn-view btn-sm">View</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Total Employee Ends >============ -->
    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

    <script src="<?= $base_url ?>public/js/bootstrapDatepickr-1.0.0.min.js"></script>
    <script src="<?= $base_url ?>public/js/jquery.dataTables.min.js"></script>
    <script src="<?= $base_url ?>public/js/dataTables.bootstrap.min.js"></script>
    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function(){
        $('#dataTable').DataTable();

        $("#date").bootstrapDatepickr({
            date_format: "d-m-Y"
        });
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>