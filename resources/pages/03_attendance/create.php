<?php 
    $myRoot = $_SERVER["DOCUMENT_ROOT"]; 
    include($myRoot . '/savasaachi_hr/resources/includes/header.php');
    include($myRoot . '/savasaachi_hr/resources/includes/style_start.php');
?>

<link rel="stylesheet" href="<?= $base_url ?>public/css/bootstrapDatepickr-1.0.0.min.css">

<style>
    /* ========< External CSS >======== */
    .card-body {
        padding-bottom: 0px;
    }

    .card-header,
    .card-footer {
        background: #4d0071;
    }

    .card-footer {
        padding-top: 5px;
    }

    .card-header h4 {
        color: #ffffff;
        padding-top: 4px;
    }

    .card-header h6 {
        color: #ffffff;
        text-align: left;
        text-transform: uppercase;
    }

    .card-header.date-filter {
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .form-control {
        border-color: #ffffff;
        height: calc(2.25rem + -1px);
    }

    .form-control:focus {
        border-color: #fff;
        border-left: 1px solid #fff;
        transition: 0.5s all ease-in-out;
    }

    .form-control.form-custom {
        border-color: #ddd;
        height: calc(2.25rem + -10px);
        /* width: 70%; */
    }

    .form-control.form-custom:focus {
        border-color: #4d0071;
        border-left: 1px solid #4d0071;
        transition: 0.5s all ease-in-out;
    }

    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + -10px);
        padding-top: 0px;
        padding-bottom: 0px;
        font-size: 12px;
        font-weight: 700;
    }
    option{
        font-weight: 600;
    }

    .custom-checkbox {
        min-height: 1rem;
        padding-left: 0;
        margin-right: 0;
        cursor: pointer;
    }

    .th-name{
        width: 20%;
    }
    .th-status, .th-holiday{
        width: 10%;
    }
    .th-leave{
        width: 15%;
    }
    .th-check_in, .th-check_out, .th-note{
        width: 15%;
    }

    .custom-checkbox .custom-control-indicator {
        content: "";
        display: inline-block;
        position: relative;
        width: 30px;
        height: 10px;
        background-color: #818181;
        border-radius: 15px;
        margin-right: 10px;
        -webkit-transition: background .3s ease;
        transition: background .3s ease;
        vertical-align: middle;
        margin: 0 16px;
        box-shadow: none;
    }

    .custom-checkbox .custom-control-indicator:after {
        content: "";
        position: absolute;
        display: inline-block;
        width: 18px;
        height: 18px;
        background-color: #f1f1f1;
        border-radius: 21px;
        box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.4);
        left: -2px;
        top: -4px;
        -webkit-transition: left .3s ease, background .3s ease, box-shadow .1s ease;
        transition: left .3s ease, background .3s ease, box-shadow .1s ease;
    }

    .custom-checkbox .custom-control-input:checked ~ .custom-control-indicator {
        background-color: #4d0071bf;
        background-image: none;
        box-shadow: none !important;
    }

    .custom-checkbox .custom-control-input:checked ~ .custom-control-indicator:after {
        background-color: #4d0071ed;
        left: 15px;
    }

    .custom-checkbox .custom-control-input:focus ~ .custom-control-indicator {
        box-shadow: none !important;
    }
    .bootstrapDatepickr-day:hover, .bootstrapDatepickr-next-month:hover, .bootstrapDatepickr-prev-month:hover {
        background-color: #4d0071;
        color: #fff;
    }
    </style>
<?php include($myRoot . '/savasaachi_hr/resources/includes/style_end.php'); ?>

    <!-- =============< Main Body Content Starts Here >============= -->
    <div class="container-fluid">
        <!-- Breadcrumb Starts -->
        <div class="row"> 
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/01_dashboard/index"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?= $base_url ?>resources/pages/03_attendance/index">Attendance</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Assign Today Attendance
                        </li>
                    </ol>
                </nav>
            </div>
        </div> 
        <!-- Breadcrumb Ends -->
    </div>


    <!-- ==========< Attendance Starts >=========== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h4>Daily Attendance</h4>
                        </div>
                        <a href="javascript:history.back()" class="btn btn-outline-light btn-custom float-right">Back</a>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header date-filter">
                                <h4 class="float-left">14-02-2019</h4>
                                <div class="float-right">
                                    <form class="form-inline" action="" method="post">
                                    <div class="form-group">
                                        <input type="text" id="date" name="date" class="form-control" placeholder="14-02-2019">
                                        <button type="submit" class="btn btn-light btn-custom btn-filter">Filter</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="th-name">Name</th>
                                            <th scope="col" class="th-status">Status</th>
                                            <th scope="col" class="th-check_in">Checked In</th>
                                            <th scope="col" class="th-check_out">Checked Out</th>
                                            <th scope="col" class="th-holiday">Holiday</th>
                                            <th scope="col" class="th-leave">Leave</th>
                                            <th scope="col" class="th-note">Note</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="table-row" id="row_1">
                                            <th scope="row" class="td-name">Sourav Roy</th>
                                            <td class="td-status">
                                                <label class="custom-control custom-checkbox">
                                                    <input name="status" type="checkbox" class="custom-control-input status">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            <td class="td-check_in">
                                                <input type="text" id="check_in" name="check_in" class="form-control form-custom check_in" placeholder="11:00 AM" disabled>
                                            </td>
                                            <td class="td-check_out">
                                                <input type="text" id="check_out" name="check_out" class="form-control form-custom check_out" placeholder="07:00 PM" disabled>
                                            </td>
                                            <td class="td-holiday">
                                                <label class="custom-control custom-checkbox">
                                                    <input name="holiday" type="checkbox" class="custom-control-input holiday">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            <td class="td-leave">
                                                <select id="leave" name="leave" class="form-control form-custom leave" required>
                                                    <option>Type</option>
                                                    <option value="casual_leave">Casual Leave</option>
                                                    <option value="sick_leave">Sick Leave</option>
                                                    <option value="absent">Absent Leave</option>
                                                </select>
                                            </td>
                                            <td class="td-note">
                                                <input type="text" id="note" name="note" class="form-control form-custom note" placeholder="Simple Note">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-outline-light btn-custom btn-submit float-right">Keep New Entry</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========< Attendance Ends >============ -->
    <!-- ==============< Main Body Content Ends Here >============== -->

<?php include($myRoot . '/savasaachi_hr/resources/includes/footer.php');?>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_start.php');?>

<script src="<?= $base_url ?>public/js/bootstrapDatepickr-1.0.0.min.js"></script>

    <script>
    /* ========< External JS >======== -->*/
    $(document).ready(function () {
        $('input').on('click',function () {
            var row = '#' + $(this).parent().parent().parent().attr("id");
            var status = $(row + ' .status');
            var check_in = $(row + ' .check_in');
            var check_out = $(row + ' .check_out');
            var holiday = $(row + ' .holiday');
            var leave = $(row + ' .leave');

            if (status.is(':checked')) {
                check_in.removeAttr('disabled', 'disabled');
                check_out.removeAttr('disabled', 'disabled');
                holiday.attr('disabled', 'disabled');
                leave.attr('disabled', 'disabled');
            } else {
                check_in.attr('disabled', 'disabled');
                check_out.attr('disabled', 'disabled');
                holiday.removeAttr('disabled', 'disabled');
                leave.removeAttr('disabled', 'disabled');
            }
        });

        // Date Picker
        $("#date").bootstrapDatepickr({
            date_format: "d-m-Y"
        });
    });
    </script>
<?php include($myRoot . '/savasaachi_hr/resources/includes/script_end.php'); ?>