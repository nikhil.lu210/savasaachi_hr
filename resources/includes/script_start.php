            </div>
            
            <?php include 'footer.php'; ?>
        </div>        
    </div>
    
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= $base_url ?>public/js/jquery.min.js"></script>
    <script src="<?= $base_url ?>public/js/popper.min.js"></script>
    <script src="<?= $base_url ?>public/js/bootstrap.min.js"></script>


    <!-- Other Global JS/Jqueries -->
    <script src="<?= $base_url ?>public/js/carbon/carbon.js"></script>
    <script src="<?= $base_url ?>public/js/carbon/demo.js"></script>
    <script src="<?= $base_url ?>public/js/script.js"></script>

    <!-- Custom JS -->