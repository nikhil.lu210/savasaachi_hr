<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">Navigation</li>

            <!-- Dashboard -->
            <li class="nav-item">
                <a href="<?= $base_url ?>resources/pages/01_dashboard/index" class="nav-link">
                    <i class="fas fa-desktop"></i> Dashboard
                </a>
            </li>

            <!-- Employee -->
            <li class="nav-item">
                <a href="<?= $base_url ?>resources/pages/02_employee/index" class="nav-link">
                    <i class="fas fa-users"></i> Employee
                </a>
            </li>

            <!-- Attendance -->
            <li class="nav-item">
                <a href="<?= $base_url ?>resources/pages/03_attendance/index" class="nav-link">
                    <i class="fas fa-fingerprint"></i> Attendance
                </a>
            </li>

            <!-- Salary -->
            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="fas fa-money-check-alt"></i> Salary <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="<?= $base_url ?>resources/pages/04_salary/04_01_all_employee/index" class="nav-link">
                            <i class="fas fa-users-cog"></i> All Employee
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $base_url ?>resources/pages/04_salary/04_02_salary_control/index" class="nav-link">
                            <i class="fas fa-hand-holding-usd"></i> Salary Control
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-title">Settings</li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="fas fa-cogs"></i> Controls <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="<?= $base_url ?>resources/pages/05_controls/05_01_department/index" class="nav-link">
                            <i class="fas fa-bezier-curve"></i> Department
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $base_url ?>resources/pages/05_controls/05_02_notice/index" class="nav-link">
                            <i class="far fa-edit"></i> Notice
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $base_url ?>resources/pages/05_controls/05_03_holiday/index" class="nav-link">
                            <i class="fab fa-fly"></i> Holiday
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $base_url ?>resources/pages/05_controls/05_04_expense/index" class="nav-link">
                            <i class="fas fa-dollar-sign"></i> Expense
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $base_url ?>resources/pages/05_controls/05_05_award/index" class="nav-link">
                            <i class="fas fa-award"></i> Award
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>