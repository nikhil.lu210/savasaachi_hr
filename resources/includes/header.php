<!doctype html>
<html lang="en">
<?php
    session_start();
    $base_url = 'http://localhost/savasaachi_hr/';
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Page Title -->
    <title>SAVASAACHI HR</title>

    <!-- Icon in Tab -->
    <link rel="shortcut icon" href="<?= $base_url ?>public/images/logo.png"> 

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= $base_url ?>public/css/bootstrap.min.css">

    <!-- Other CSS -->
    <link rel="stylesheet" href="<?= $base_url ?>public/vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= $base_url ?>public/css/carbon.css">

    <!-- Custom CSS -->
    <!-- <link rel="stylesheet" href="public/css/reset.css"> -->
    <link rel="stylesheet" href="<?= $base_url ?>public/css/style.css">
    <link rel="stylesheet" href="<?= $base_url ?>public/css/responsive.css">