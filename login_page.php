<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Page Title -->
    <title>SAVASAACHI HR</title>

    <!-- Icon in Tab -->
    <link rel="shortcut icon" href="#"> 

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="public/css/bootstrap.min.css">

    <!-- Other CSS -->
    <link rel="stylesheet" href="public/vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="public/vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="public/css/carbon.css">

    <!-- Custom CSS -->
    <!-- <link rel="stylesheet" href="public/css/reset.css"> -->
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/responsive.css">
</head>
<body>
<div class="page-wrapper flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <form action="" method="post">
                    <div class="card p-4">
                        <div class="card-header text-center text-uppercase h4 font-weight-bold">
                            SAVASAACHI HR LOGIN
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-control-label">Email</label>
                                <input type="email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Password</label>
                                <input type="password" class="form-control">
                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary px-5 btn-block">Login</button>
                                </div>

                                <div class="col-12">
                                    <a href="#" class="btn btn-link float-right">Forgot password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/pooper.min.js"></script>
    <script src="public/js/bootstrap.min.js"></script>


    <!-- Other Global JS/Jqueries -->
    <script src="public/js/carbon/carbon.js"></script>
    <script src="public/js/carbon/demo.js"></script>
</body>
</html>
